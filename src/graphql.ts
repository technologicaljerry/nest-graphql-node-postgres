
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface CreateUserInput {
    exampleField: number;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
}

export interface UpdateUserInput {
    exampleField?: Nullable<number>;
    firstName?: Nullable<string>;
    lastName?: Nullable<string>;
    email?: Nullable<string>;
    role?: Nullable<string>;
    userId: string;
}

export interface User {
    userId: string;
    exampleField: number;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
}

export interface IQuery {
    users(): User[] | Promise<User[]>;
    user(userId: string): User | Promise<User>;
}

export interface IMutation {
    createUser(createUserInput: CreateUserInput): User | Promise<User>;
    updateUser(updateUserInput: UpdateUserInput): User | Promise<User>;
    removeUser(userId: string): User | Promise<User>;
}

type Nullable<T> = T | null;
